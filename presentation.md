---
marp: true
theme: uncover
class: invert
invert-theme: true
---

## Predicting research article impact 
##### A machine learning approach for estimating citations

<br>

*Athul Dev Sudhakar Ponnu, Tobias Stein, <br> Karsten Schimpf*

---

> A Mathematical Model for the Determination of Total Area Under Glucose Tolerance and Other Metabolic Curves

> [...] In Tai's Model, the total area under a curve is computed by dividing the area under the curve between two designated values on the X-axis (abscissas) into small segments (rectangles and triangles) whose areas can be accurately calculated from their respective geometrical formulas. [...]

*1994, Diabetes Care*

<!-- Citation Count Crossref: 332 -->
<!-- Citation Count GScholar: 535 -->

---

![bg width:200 left](https://assets.crossref.org/logo/crossref-logo-200.svg)

### The Dataset

* April 2023 Public Data
* 143,500,000 Entries
* 73 Features
* ~ 185.9 GB

---

<!-- header: <h1 style="text-align: top; color: #ffffff"> The problem </h1> -->

<img src="https://mlho-article-prediction.pages.gwdg.de/main/presentation/problem/problem.svg">

---

<!-- header: <h1 style="text-align: top; color: #ffffff"> Pipeline </h1> -->

<img src="https://mlho-article-prediction.pages.gwdg.de/main/presentation/input-pipeline/input-pipeline.svg">

<!-- footer: <img src="https://raw.githubusercontent.com/rust-lang/rust-artwork/bf0b3272f9ba8d22f7fd45e408496d05621b3b5c/logo/rust-logo-blk.svg" style="filter: invert(1)" width=50> -->

---

<!-- header: <h1 style="text-align: top; color: #ffffff"> Features </h1> -->

<img src="https://mlho-article-prediction.pages.gwdg.de/main/presentation/feature_abundance/feature_abundance.png">

---

<!-- header: <h1 style="text-align: top; color: #ffffff"> Data-Structure </h1> -->

```rust
#[derive(Debug, serde::Serialize, serde::Deserialize)]
pub struct Article
{
    title: String,
    container_title: String,
    r#type: String,
    is_referenced_by_count: u64,
    reference_count: u64,
    published: u64,
    author_count: usize,
    r#abstract: String
}
```

---

<!-- header: <h1 style="text-align: top; color: #ffffff"> Pipeline </h1> -->

<img src="https://mlho-article-prediction.pages.gwdg.de/main/presentation/input-pipeline/input-pipeline.svg">

<!-- footer: <img src="https://raw.githubusercontent.com/rust-lang/rust-artwork/bf0b3272f9ba8d22f7fd45e408496d05621b3b5c/logo/rust-logo-blk.svg" style="filter: invert(1)" width=50> -->

---

<!-- header: <h1 style="text-align: top; color: #ffffff"> Citation Count </h1> -->

![height:650px](https://mlho-article-prediction.pages.gwdg.de/main/presentation/data_stats/citations.png)

<!-- footer: "" -->

---

![height:650px](https://mlho-article-prediction.pages.gwdg.de/main/presentation/data_stats/citations_100.png)

---

<!-- header: <h1 style="text-align: top; color: #ffffff"> Published Year </h1> -->

![height:650px](https://mlho-article-prediction.pages.gwdg.de/main/presentation/data_stats/published.png)

---

<!-- header: <h1 style="text-align: top; color: #ffffff"> Pipeline </h1> -->

<img src="https://mlho-article-prediction.pages.gwdg.de/main/presentation/input-pipeline/input-pipeline.svg">

<!-- footer: <img src="https://raw.githubusercontent.com/rust-lang/rust-artwork/bf0b3272f9ba8d22f7fd45e408496d05621b3b5c/logo/rust-logo-blk.svg" style="filter: invert(1)" width=50> -->

---

<!-- header: <h1 style="text-align: top; color: #ffffff"> Pipeline result </h1> -->

![width:800px](https://mlho-article-prediction.pages.gwdg.de/main/presentation/pipeline_result/result.svg)

<!-- footer: <img src="https://raw.githubusercontent.com/rust-lang/rust-artwork/bf0b3272f9ba8d22f7fd45e408496d05621b3b5c/logo/rust-logo-blk.svg" style="filter: invert(1)" width=50> -->

---

<!-- header: "" -->

### Bias

<!-- footer: "" -->

---

<!-- header: <h1 style="text-align: top; color: #ffffff"> Title </h1> -->

![width:800px](https://mlho-article-prediction.pages.gwdg.de/main/presentation/word_clouds/title.png)



---

<!-- header: <h1 style="text-align: top; color: #ffffff"> Abstract </h1> -->

![width:800px](https://mlho-article-prediction.pages.gwdg.de/main/presentation/word_clouds/abstract.png)



---

<!-- header: <h1 style="text-align: top; color: #ffffff"> Container Title (Split) </h1> -->

![width:800px](https://mlho-article-prediction.pages.gwdg.de/main/presentation/word_clouds/publisher_split.png)

---

<!-- header: <h1 style="text-align: top; color: #ffffff"> Article Type </h1> -->
![width:900px](https://mlho-article-prediction.pages.gwdg.de/main/presentation/correlation/type.png)

---

<!-- header: <h1 style="text-align: top; color: #ffffff">  </h1> -->

![bg width:1200px](https://mlho-article-prediction.pages.gwdg.de/main/presentation/model-overview/model-overview.svg)

### The Models

---

# NLP models

---
<!-- header: <h1 style="text-align: top; color: #ffffff"> Data Preprocessing </h1> -->


- Data sampling (every 10th entry)
- One abstract model and one journal title model
- Citation classes
- 1-10, 11-100, 101-1000, 1001-$\infty$
- CountVectorizer (RAM bottleneck)
- Train-Test-split 80-20
- 50% undersampling
- Training 14 models

---

<!-- header: <h1 style="text-align: top; color: #ffffff"> Model Architecture </h1> -->

- Training with 10 epochs, batch size of 32, and a 80-20 train-validation-split

![bg left width:500px](https://pad.gwdg.de/uploads/8ccd488f-f2db-4cdd-92ca-e3d7de67326b.png)


---

<!-- header: <h1 style="text-align: top; color: #ffffff"> Bias Initialization </h1> --> 

- Output layer initial bias: `bias = log(P / N)`
- `P` positive examples
- `N` negative examples

https://towardsdatascience.com/solving-the-class-imbalance-problem-58cb926b5a0f

---

<!-- header: <h1 style="text-align: top; color: #ffffff"> Evaluation </h1> --> 

- Adding the activations of both models and taking the maximum

![bg left width:700px](https://pad.gwdg.de/uploads/e5ea97d9-39de-4686-bfa5-83b24785fc04.png)



- Average accuracy 67%
- Always guessing the lowest class 62%


---

<!-- header: <h1 style="text-align: top; color: #ffffff"> Worse models </h1> --> 

- K-Nearest Neighbors
- Random Forest
- Linear Regression
- Transformer (transfer learning, distilbert-base-uncased)

---

<!-- header: <h1 style="text-align: top; color: #ffffff"> </h1> --> 


# No Significant Difference

---

<!-- header: <h1 style="text-align: top; color: #ffffff"> 1. Other Regularizations </h1> --> 


- L1  vs. L2

---

<!-- header: <h1 style="text-align: top; color: #ffffff"> 2. Layer Sizes and Amounts </h1> --> 

- Original hidden layers: 64, 32
- More layers: 128, 64, 32
- Bigger layers: 128, 64

---

<!-- header: <h1 style="text-align: top; color: #ffffff"> 3. RMSE Instead of Classes </h1> --> 

- Same model with different last layer
- Last layer only one neuron with linear activation
- RMSE=87 for always predicting 1 citation

---

<!-- header: <h1 style="text-align: top; color: #ffffff"> 4. Weighted Cost Function </h1> -->

- `weight = T/P`
- `T` total number
- `P` positive examples

---

<!-- header: <h1 style="text-align: top; color: #ffffff"> 5. Predicting citations after 2 years </h1> -->

- Limiting the data to papers from 2021

---

<!-- header: <h1 style="text-align: top; color: #ffffff"> 6. Using relationships between words </h1> -->

- Training a Word2Vec model (no improvement)
- Using GloVe

---

<!-- header: <h1 style="text-align: top; color: #ffffff"> 7. Using different classes </h1> -->

![bg width:700px](https://pad.gwdg.de/uploads/bcdf0058-d036-4665-b5db-18edfcfb1c8a.png)


---

<!-- header: <h1 style="text-align: top; color: #ffffff"> Ensemble Model </h1> -->

- Code compatibility issues
- Software engineering is hard

---

<!-- header: <h1 style="text-align: top; color: #ffffff"> Conclusion </h1> -->

- We could not overcome the class inbalance
- Real data is hard