from typing import List

import os
import pandas as pd
from fastprogress.fastprogress import progress_bar


def load_data_from_directory(dir_path: str, columns: List[str] = None) -> pd.DataFrame:
    file_paths = [os.path.join(dir_path, x) for x in os.listdir(dir_path)]

    data = pd.read_csv(file_paths[0], usecols=columns)
    for file_path in progress_bar(file_paths[1:]):
        data = pd.concat((data, pd.read_csv(file_path, usecols=columns)), axis=0)

    return data


if __name__ == "__main__":
    data = load_data_from_directory("/run/media/wasserstoff/Fedora Linux New/torrent/filtered")
    print(data.head())
    input()
