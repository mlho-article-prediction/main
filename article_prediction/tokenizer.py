import json

import numpy as np
import pandas as pd
from sklearn.ensemble import RandomForestClassifier
from sklearn.feature_extraction.text import CountVectorizer


def main():
    data = pd.read_csv("data/train.csv")

    data.dropna(inplace=True)

    X_train = data["title"] + " " + data["abstract"]
    y_train = data["is_referenced_by_count"]

    vectorizer = CountVectorizer(max_features=1000, stop_words='english')
    X_train_vec = vectorizer.fit_transform(X_train)

    #np.save("data/x_train.npy", np.array(X_train_vec))

    with open("data/y_train.txt", "w") as file:
        for x in y_train:
            file.write(f"{x}\n")


if __name__ == '__main__':
    main()
