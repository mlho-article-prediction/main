import pickle

import numpy as np
from joblib import dump
from sklearn.ensemble import RandomForestClassifier


def main():
    with open("data/x_train.txt", "rb") as fin:
        X_train = pickle.load(fin)

    with open("data/y_train.txt", "r") as file:
        y_train = np.array([int(x) for x in file.readlines()])

    rf_classifier = RandomForestClassifier(n_estimators=100, random_state=42, n_jobs=12)
    rf_classifier.fit(X_train, y_train)

    dump(rf_classifier, "data/rf_classifier.joblib", compress=3)


if __name__ == '__main__':
    main()
