mod article;
mod sanitiser;
mod filter;
mod string_standardiser;
mod process_data;
mod word_occurrence;
mod file_balancing;
mod test_train_split;

mod word_correlation;
mod full_stats;

use std::collections::HashMap;
use pyo3::methods::OkWrap;
use pyo3::prelude::*;

use crate::process_data::process_data;
use crate::test_train_split::test_train_split;
use crate::word_occurrence::{word_occurrence, word_occurrence_in_dir};
use crate::file_balancing::rebalance_dir;

use crate::word_correlation::word_correlation::word_citation_correlation;
use crate::word_correlation::correlation::correlation;

use crate::full_stats::FullStats;
use crate::full_stats::calc_full_stats_dir;

#[pyfunction]
fn calc_full_stats(input_dir: String) -> FullStats
{
    calc_full_stats_dir(input_dir)
}

#[pyfunction]
fn process_directory(input_dir: String, output_dir: String) {
    process_data(input_dir, output_dir)
}

#[pyfunction]
fn rebalance_directory(input_dir: String, output_dir: String)
{
    rebalance_dir(input_dir, output_dir)
}

#[pyfunction]
fn train_test_split_directory(input_dir: String, output_dir: String, size: f64)
{
    test_train_split(input_dir, output_dir, size)
}

#[pyfunction]
fn calc_word_occurrence(input_path: String, field: String, split: bool) -> HashMap<String, u64>
{
    word_occurrence(&input_path, &field, split)
}

#[pyfunction]
fn calc_word_occurrence_in_dir(input_dir: String, field: String, split: bool) -> HashMap<String, u64>
{
    word_occurrence_in_dir(&input_dir, &field, split)
}

#[pyfunction]
fn calc_word_citation_correlation(input_dir: String, field: String, split: bool) -> HashMap<String, f64>
{
    word_citation_correlation(&input_dir, &field, split)
}

#[pyfunction]
fn calc_correlation(x: Vec<u64>, y: Vec<u64>) -> f64
{
    correlation(&x, &y)
}

#[pymodule]
fn crossref(_py: Python<'_>, m: &PyModule) -> PyResult<()> {
    m.add_function(wrap_pyfunction!(calc_full_stats, m)?)?;
    m.add_function(wrap_pyfunction!(process_directory, m)?)?;
    m.add_function(wrap_pyfunction!(rebalance_directory, m)?)?;
    m.add_function(wrap_pyfunction!(train_test_split_directory, m)?)?;
    m.add_function(wrap_pyfunction!(calc_word_occurrence, m)?)?;
    m.add_function(wrap_pyfunction!(calc_word_occurrence_in_dir, m)?)?;
    m.add_function(wrap_pyfunction!(calc_word_citation_correlation, m)?)?;
    m.add_function(wrap_pyfunction!(calc_correlation, m)?)?;
    Ok(())
}