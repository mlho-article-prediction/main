mod article;
mod sanitiser;
mod filter;
mod process_data;
mod word_occurrence;
mod file_balancing;
mod test_train_split;
mod word_correlation;
mod string_standardiser;
mod feature_occurence;
mod full_stats;


use std::env;
use std::time::Instant;

use crate::process_data::process_data;

fn main() {
    let start = Instant::now();

    let args: Vec<String> = env::args().collect();

    if args.len() != 3
    {
        panic!("Input and output directory have to specified!")
    }

    let input_dir: String = args[1].clone();
    let output_dir: String = args[2].clone();

    process_data(input_dir, output_dir);

    println!("Elapsed: {:?}", start.elapsed());
}
