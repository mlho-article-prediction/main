use itertools::Itertools;

use std::{thread};
use flate2::bufread;
use std::fs::File;
use std::io::{BufReader, Read};
use serde_json::{Value};
use std::thread::JoinHandle;
use crate::article::Article;
use crate::filter::{lang_filter::LangFilter, citation_filter::CitationFilter, published_filter::PublishedFilter, filter_pipeline::FilterPipeline, abstract_filter::AbstractFilter};
use crate::sanitiser::Sanitiser;
use crate::string_standardiser::StringStandardiser;

pub fn load_data(path: String) -> String
{
    let file = File::open(path).unwrap();
    let mut reader = BufReader::new(file);

    let mut buffer: Vec<u8> = Vec::new();
    reader.read_to_end(&mut buffer).unwrap();

    let mut gz = bufread::GzDecoder::new(&buffer[..]);
    let mut s = String::new();
    gz.read_to_string(&mut s).unwrap();

    s
}

fn build_filter_pipeline() -> FilterPipeline
{
    let abstract_filter = AbstractFilter::new();
    let lang_filter = LangFilter::new();
    let citation_filter = CitationFilter::new(1);
    let published_filter = PublishedFilter::new(1900, 2023);
    let mut filter_pipeline = FilterPipeline::new();
    filter_pipeline.add_filter(Box::new(abstract_filter));
    filter_pipeline.add_filter(Box::new(citation_filter));
    filter_pipeline.add_filter(Box::new(published_filter));
    filter_pipeline.add_filter(Box::new(lang_filter));


    filter_pipeline
}

fn parse_files(paths: Vec<String>, index: usize, output_dir: String)
{
    let sanitiser = Sanitiser::new();
    let standardiser = StringStandardiser::new();
    let filter_pipeline = build_filter_pipeline();

    let mut wrt = csv::Writer::from_path(format!("{}/{}.csv", output_dir, index)).unwrap();

    for path in paths
    {
        let data = load_data(path);

        let data: Value = serde_json::from_str(&data).unwrap();
        let articles = &data["items"];

        for article_json in articles.as_array().unwrap()
        {
            let mut article = match Article::new_from_data(article_json)
            {
                Some(x) => x,
                None => continue
            };

            article.sanitise(&sanitiser);

            article = match filter_pipeline.run(article)
            {
                Some(x) => x,
                None => continue
            };

            standardiser.standardise_article(&mut article);

            wrt.serialize(article).unwrap();
        }
    }
    wrt.flush().unwrap();
}


pub fn process_data(input_dir: String, output_dir: String) {
    let paths: Vec<String> = (0..28700).map(|x| format!("{}/{}.json.gz", input_dir, x).to_string()).collect();

    let mut handles: Vec<JoinHandle<()>> = vec![];
    let mut index = 0;
    for path_chunk in &paths.into_iter().chunks(28700/14)
    {
        index = index + 1;

        let paths: Vec<String> = path_chunk.collect();
        let index = handles.len();
        let dir = output_dir.clone();
        let handle = thread::spawn(move || {
            parse_files(paths, index, dir);
        });

        handles.push(handle);
    }

    for handle in handles
    {
        handle.join().unwrap();
    }
}