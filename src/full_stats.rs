use std::collections::HashMap;
use std::thread;
use std::thread::JoinHandle;
use itertools::Itertools;
use serde_json::Value;
use crate::process_data::load_data;

use pyo3::pyclass;

#[pyclass]
pub struct FullStats
{
    #[pyo3(get)]
    count: usize,
    #[pyo3(get, set)]
    features: HashMap<String, u64>
}

impl FullStats {
    pub fn new() -> FullStats
    {
        let count = 0;
        let features: HashMap<String, u64> = HashMap::new();

        FullStats {count, features}
    }

    pub fn add_counts(&mut self, count: usize)
    {
        self.count += count;
    }

    pub fn add_feature(&mut self, feature: String)
    {
        self.features.entry(feature).and_modify(|x| *x += 1).or_insert(0);
    }

    pub fn merge(&mut self, other: FullStats)
    {
        self.count += other.count;

        for (key, value) in &other.features
        {
            self.features.entry(key.to_string()).and_modify(|x| *x += *value).or_insert(*value);
        }
    }
}

fn parse_files(paths: Vec<String>) -> FullStats
{
    let mut stats = FullStats::new();
    for path in paths
    {
        let data = load_data(path);

        let data: Value = serde_json::from_str(&data).unwrap();
        let articles = &data["items"];

        stats.add_counts(articles.as_array().unwrap().len());

        for article in articles.as_array().unwrap()
        {
            for features in article.as_object().unwrap().keys()
            {
                stats.add_feature(features.to_string());
            }
        }
    }
    stats
}


pub fn calc_full_stats_dir(input_dir: String) -> FullStats {
    let paths: Vec<String> = (0..28700).map(|x| format!("{}/{}.json.gz", input_dir, x).to_string()).collect();

    let mut handles: Vec<JoinHandle<FullStats>> = vec![];
    let mut index = 0;
    for path_chunk in &paths.into_iter().chunks(28700/14)
    {
        index = index + 1;

        let paths: Vec<String> = path_chunk.collect();

        let handle: JoinHandle<FullStats> = thread::spawn(move || {
            parse_files(paths)
        });

        handles.push(handle);
    }

    let mut result = FullStats::new();

    for handle in handles
    {
        result.merge(handle.join().unwrap());
    }

    result
}