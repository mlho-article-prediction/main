use std::collections::HashMap;


struct FeatureOccurrence
{
    features: HashMap<String, u64>
}

impl FeatureOccurrence
{
    pub fn new() -> FeatureOccurrence
    {
        let features: HashMap<String, u64> = HashMap::new();
        FeatureOccurrence {features}
    }
}