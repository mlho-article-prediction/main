

pub fn correlation(x_list: &Vec<u64>, y_list: &Vec<u64>) -> f64
{
    let length: f64 = x_list.len() as f64;
    let mut x_sum: u64 = 0;
    let mut y_sum: u64 = 0;
    let mut x_sq_sum: u64 = 0;
    let mut y_sq_sum: u64 = 0;
    let mut xy_sum: u64 = 0;

    for (x, y) in x_list.iter().zip(y_list.iter())
    {
        x_sum += x;
        y_sum += y;
        x_sq_sum += x.pow(2);
        y_sq_sum += y.pow(2);
        xy_sum += x * y;
    }
    
    let x_var: f64 = length * (x_sq_sum as f64) - (x_sum.pow(2) as f64);
    let y_var: f64 = length * (y_sq_sum as f64) - (y_sum.pow(2) as f64);

     (length * (xy_sum as f64) - ((x_sum * y_sum) as f64))/(x_var.sqrt() * y_var.sqrt())
}