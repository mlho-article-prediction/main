pub struct CitationTracker
{
    n: u64,
    sum: u64,
    sq_sum: u64
}

impl CitationTracker
{
    pub fn new(value: u64) -> CitationTracker
    {
        let mut tracker = CitationTracker{n: 0, sum: 0, sq_sum: 0};
        tracker.add(value);
        tracker
    }

    pub fn add(&mut self, value: u64)
    {
        self.n += 1;
        self.sum += value;
        self.sq_sum += value.pow(2);
    }

    pub fn merge(&mut self, other: &CitationTracker)
    {
        self.n += other.n;
        self.sum += other.sum;
        self.sq_sum += other.sq_sum;
    }

    pub fn get_n(&self) -> u64
    {
        self.n
    }

    pub fn get_sum(&self) -> u64
    {
        self.sum
    }

    pub fn get_sq_sum(&self) -> u64
    {
        self.sq_sum
    }
}


#[derive(Clone, Copy)]
pub struct CorrelationCalculator
{
    sum: u64,
    sq_sum: u64,
    cross_sum: u64
}

impl CorrelationCalculator
{
    pub fn new(x: u64, y: u64) -> CorrelationCalculator
    {
        let mut calc = CorrelationCalculator{sum: 0, sq_sum: 0, cross_sum: 0};
        calc.add(x, y);
        calc
    }

    pub fn add(&mut self, x: u64, y: u64)
    {
        self.sum += x;
        self.sq_sum += x.pow(2);
        self.cross_sum += x * y;
    }

    pub fn calc(&self, tracker: &CitationTracker) -> f64
    {
        let length: f64 = tracker.n as f64;

        let x_var: f64 = length * (self.sq_sum as f64) - (self.sum.pow(2) as f64);
        let y_var: f64 = length * (tracker.sq_sum as f64) - (tracker.sum.pow(2) as f64);

        if  ((length * (self.cross_sum as f64) - ((self.sum * tracker.sum) as f64))/(x_var.sqrt() * y_var.sqrt())).is_nan()
        {
            //panic!("{} {} {} {} {} {}", self.sum, self.sq_sum, self.cross_sum, tracker.sum, tracker.sq_sum, tracker.n);
        }

        (length * (self.cross_sum as f64) - ((self.sum * tracker.sum) as f64))/(x_var.sqrt() * y_var.sqrt())
    }

     pub fn merge(&mut self, other: &CorrelationCalculator)
     {
         self.sum += other.sum;
         self.sq_sum += other.sq_sum;
         self.cross_sum += other.cross_sum;
    }

    pub fn get_sum(&self) -> u64
    {
        self.sum
    }

    pub fn get_sq_sum(&self) -> u64
    {
        self.sq_sum
    }

    pub fn get_cross_sum(&self) -> u64
    {
        self.cross_sum
    }

}


pub struct CombinedCorrelationCalculator
{
    pub correlation_calculator: CorrelationCalculator,
    pub citation_tracker: CitationTracker
}

impl CombinedCorrelationCalculator
{
     pub fn new(x: u64, y: u64) -> CombinedCorrelationCalculator
    {
        let correlation_calculator = CorrelationCalculator::new(x, y);
        let citation_tracker = CitationTracker::new(y);
        CombinedCorrelationCalculator {correlation_calculator, citation_tracker}
    }

    pub fn add(&mut self, x: u64, y: u64)
    {
        self.correlation_calculator.add(x, y);
        self.citation_tracker.add(y);
    }

    pub fn calc(&self) -> f64
    {
        self.correlation_calculator.calc(&self.citation_tracker)
    }

    pub fn merge(&mut self, other: &CombinedCorrelationCalculator)
    {
        self.citation_tracker.merge(&other.citation_tracker);
        self.correlation_calculator.merge(&other.correlation_calculator);
    }
}