use std::collections::HashMap;
use std::str::Split;
use std::thread;
use std::thread::JoinHandle;
use crate::article::Article;
use crate::word_correlation::correlation::correlation;
use crate::word_correlation::correlation_calculator::{CitationTracker, CombinedCorrelationCalculator, CorrelationCalculator};

struct WordCitationOccurrence
{
    //citations: CitationTracker,
    words: HashMap<String, CombinedCorrelationCalculator>
}

impl WordCitationOccurrence
{
    fn new() -> WordCitationOccurrence
    {
        //let citations: CitationTracker = CitationTracker::new(0);
        let words: HashMap<String, CombinedCorrelationCalculator> = HashMap::new();

        WordCitationOccurrence {words}
    }

    fn add_article(&mut self, article: Article, field: &String, split: bool)
    {
        let field = article.get_field_by_string(&field);
        //self.citations.add(article.get_citations());

        let words: Split<&str>;
        if split
        {
            words = field.split(" ");
        }else
        {
            words = field.split("\n");
        }

        let mut occurrences: HashMap<String, u64> = HashMap::new();
        for word in words
        {
            *occurrences.entry(word.to_string().to_lowercase()).or_insert(0) += 1;
        }

        for (key, value) in &occurrences
        {
            self.words.entry(key.to_string()).and_modify(|x| x.add(*value, article.get_citations())).or_insert(CombinedCorrelationCalculator::new(*value, article.get_citations()));
        }
    }

    fn merge(&mut self, mut other: WordCitationOccurrence)
    {
        //self.citations.merge(&other.citations);
        for (key, value) in other.words
        {
            self.words.entry(key.to_string()).and_modify(|x| x.merge(&value)).or_insert(value);
        }
    }
}

fn word_citation_occurrence(input_path: &String, field: &String, split: bool) -> WordCitationOccurrence
{
    let mut occurrences = WordCitationOccurrence::new();
    let mut reader = csv::Reader::from_path(input_path).unwrap();

    for result in reader.deserialize()
    {
        let article: Article = result.unwrap();
        occurrences.add_article(article, field, split);


    }

    return occurrences
}

fn word_citation_occurrence_dir(input_dir: &String, field: &String, split: bool) -> WordCitationOccurrence
{
    let paths: Vec<String> = (0..1).map(|x| format!("{}/{}.csv", input_dir, x).to_string()).collect();

    let mut handles: Vec<JoinHandle<WordCitationOccurrence>> = vec![];
    for path in paths
    {
        let f = field.clone();
        let p = path.clone();
        let handle: JoinHandle<WordCitationOccurrence> = thread::spawn(move || {
            word_citation_occurrence(&p, &f, split)
        });

        handles.push(handle);
    }

    let mut merged_occurrences: WordCitationOccurrence = WordCitationOccurrence::new();

    for handle in handles
    {
        merged_occurrences.merge(handle.join().unwrap());
    }
    return merged_occurrences
}

pub fn word_citation_correlation(input_dir: &String, field: &String, split: bool) -> HashMap<String, f64>
{
    let occurrences = word_citation_occurrence_dir(input_dir, field, split);
    let mut correlations: HashMap<String, f64> = HashMap::new();

    for (key, value) in &occurrences.words
    {
        correlations.insert(key.to_string(), value.calc()); //&occurrences.citations
    }

    correlations
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_add_single() {
        let mut occurrence = WordCitationOccurrence::new();

        let nature_article = Article::new(
            "Lorem ipsum sum".to_string(),
            "Nature".to_string(),
            10,
            10,
            1980,
            1,
            "Lorem ipsum sum".to_string(),
        );

        occurrence.add_article(nature_article, &"container_title".to_string(), false);

        assert_eq!(occurrence.words["nature"].citation_tracker.get_n(), 1);
        assert_eq!(occurrence.words["nature"].correlation_calculator.get_sum(), 1);
        assert_eq!(occurrence.words["nature"].correlation_calculator.get_sq_sum(), 1);
        assert_eq!(occurrence.words["nature"].correlation_calculator.get_cross_sum(), 10);
    }

    #[test]
    fn test_add_double() {
        let mut occurrence = WordCitationOccurrence::new();

        let nature_article = Article::new(
            "Lorem ipsum sum".to_string(),
            "Nature".to_string(),
            10,
            10,
            1980,
            1,
            "Lorem ipsum sum".to_string(),
        );

        let another_article = Article::new(
            "Lorem ipsum sum".to_string(),
            "Nature".to_string(),
            1,
            10,
            1980,
            1,
            "Lorem ipsum sum".to_string(),
        );

        occurrence.add_article(nature_article, &"container_title".to_string(), false);
        occurrence.add_article(another_article, &"container_title".to_string(), false);

        assert_eq!(occurrence.words["nature"].citation_tracker.get_n(), 2);
        assert_eq!(occurrence.words["nature"].correlation_calculator.get_sum(), 2);
        assert_eq!(occurrence.words["nature"].correlation_calculator.get_sq_sum(), 2);
        assert_eq!(occurrence.words["nature"].correlation_calculator.get_cross_sum(), 11);
    }

    #[test]
    fn test_add_different() {
        let mut occurrence = WordCitationOccurrence::new();

        let nature_article = Article::new(
            "Lorem ipsum sum".to_string(),
            "Nature".to_string(),
            10,
            10,
            1980,
            1,
            "Lorem ipsum sum".to_string(),
        );

        let science_article = Article::new(
            "Lorem ipsum sum".to_string(),
            "Science".to_string(),
            5,
            10,
            1980,
            1,
            "Lorem ipsum sum".to_string(),
        );

        occurrence.add_article(nature_article, &"container_title".to_string(), false);
        occurrence.add_article(science_article, &"container_title".to_string(), false);

        assert_eq!(occurrence.words["nature"].citation_tracker.get_n(), 1);
        assert_eq!(occurrence.words["nature"].correlation_calculator.get_sum(), 1);
        assert_eq!(occurrence.words["nature"].correlation_calculator.get_sq_sum(), 1);
        assert_eq!(occurrence.words["nature"].correlation_calculator.get_cross_sum(), 10);

        assert_eq!(occurrence.words["science"].citation_tracker.get_n(), 1);
        assert_eq!(occurrence.words["science"].correlation_calculator.get_sum(), 1);
        assert_eq!(occurrence.words["science"].correlation_calculator.get_sq_sum(), 1);
        assert_eq!(occurrence.words["science"].correlation_calculator.get_cross_sum(), 5);
    }
}