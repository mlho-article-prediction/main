use std::fmt;
use serde_json::Value;
use crate::sanitiser::Sanitiser;

#[derive(Debug, serde::Serialize, serde::Deserialize)]
pub struct Article
{
    title: String,
    container_title: String,
    r#type: String,
    is_referenced_by_count: u64,
    reference_count: u64,
    published: u64,
    author_count: usize,
    r#abstract: String
}

impl Article
{
    pub fn new(title: String, container_title: String, r#type: String, is_referenced_by_count: u64, reference_count: u64, published: u64,
    author_count: usize, r#abstract: String) -> Article
    {
        Article {title, container_title, r#type, is_referenced_by_count, reference_count, published, author_count, r#abstract}
    }

    pub fn new_from_data(input: &Value) -> Option<Article>
    {
        let title: &str = input["title"][0].as_str()?;
        let container_title: &str = input["container-title"][0].as_str()?;
        let r#type: &str = input["type"].as_str()?;
        let is_referenced_by_count: u64 = input["is-referenced-by-count"].as_u64()?;
        let reference_count: u64 = input["reference-count"].as_u64()?;
        let published: u64 = input["published"]["date-parts"][0][0].as_u64()?;
        let author_count: usize = input["author"].as_array()?.len();
        let r#abstract: &str = input["abstract"].as_str()?;

        Some(Article
            {
                title: title.to_string(),
                container_title: container_title.to_string(),
                r#type: r#type.to_string(),
                is_referenced_by_count,
                reference_count,
                published,
                author_count,
                r#abstract: r#abstract.to_string()
            })
    }

    pub fn get_title(&self) -> &String
    {
        &self.title
    }

    pub fn set_title(&mut self, title: String)
    {
        self.title = title;
    }

    pub fn get_published(&self) -> u64
    {
        self.published
    }

    pub fn get_citations(&self) -> u64 {self.is_referenced_by_count}

    pub fn get_abstract(&self) -> &String
    {
        &self.r#abstract
    }

    pub fn set_abstract(&mut self, r#abstract: String)
    {
        self.r#abstract = r#abstract;
    }

    pub fn get_field_by_string(&self, field: &String) -> &String
    {
        match field.as_str() {
            "title" => &self.title,
            "container_title" => &self.container_title,
            "abstract" => &self.r#abstract,
            "type" => &self.r#type,
            _ => panic!("Invalid field name!")
        }
    }

    pub fn sanitise(&mut self, sanitiser: &Sanitiser)
    {
        self.title = sanitiser.sanitise(&self.title);
        self.container_title = sanitiser.sanitise(&self.container_title);
        self.r#abstract = sanitiser.sanitise(&self.r#abstract);
    }
}