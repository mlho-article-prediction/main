use regex::Regex;
use crate::article::Article;

pub struct StringStandardiser
{
    characters: Regex,
    numbers: Regex,
    whitespaces: Regex
}

impl StringStandardiser
{
    pub fn new() -> StringStandardiser
    {
        let characters =  Regex::new(r"[^\w\s\']").unwrap();
        let numbers = Regex::new(r"\d").unwrap();
        let whitespaces = Regex::new(r"( {2,})").unwrap();

        StringStandardiser {characters, numbers, whitespaces}
    }

    fn standardise_text(&self, text: &String) -> String
    {
        let standardised = self.characters.replace_all(text, " ");
        let wo_numbers = self.numbers.replace_all(&standardised, "");
        let w_cleaned = self.whitespaces.replace_all(&wo_numbers, " ");

        w_cleaned.to_string().to_lowercase()
    }

    pub fn standardise_article(&self, article: &mut Article)
    {
        article.set_title(self.standardise_text(article.get_title()));
        article.set_abstract(self.standardise_text(article.get_abstract()));
    }
}
