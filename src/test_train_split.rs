use std::fs::File;
use std::io::{BufReader, Write, Lines, BufRead};
use std::path::Path;
use rand::distributions::{Distribution, Uniform};

use crate::file_balancing::read_lines;

fn split_file(input_path: String, train_file: &mut File, test_file: &mut File, split: f64)
{
    let split_prob: Uniform<f64> = Uniform::new_inclusive(0.0, 1.0);

    for line in read_lines(input_path).skip(1) {
        if split_prob.sample(&mut rand::thread_rng()) < split
        {
            writeln!(train_file, "{}", line.unwrap()).unwrap();
        } else {
            writeln!(test_file, "{}", line.unwrap()).unwrap();
        }
    }
}

fn init_file(input_paths: &Vec<String>, output_file: &mut File)
{
    let header = read_lines(input_paths[0].clone()).next().unwrap().unwrap();
    writeln!(output_file, "{}", header).unwrap();

}

pub fn test_train_split(input_dir: String, output_dir: String, split: f64)
{
    let input_paths: Vec<String> = (0..14).map(|x| format!("{}/{}.csv", input_dir, x).to_string()).collect();
    let mut test_file: File = File::create(format!("{}/test.csv", output_dir).to_string()).unwrap();
    let mut train_file: File = File::create(format!("{}/train.csv", output_dir).to_string()).unwrap();

    init_file(&input_paths, &mut test_file);
    init_file(&input_paths, &mut train_file);

    for path in input_paths
    {
        split_file(path, &mut train_file, &mut test_file, split);
    }
}