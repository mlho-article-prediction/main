use 
std::collections::HashMap;
use std::hash::Hash;
use std::thread;
use std::thread::JoinHandle;
use crate::article::Article;

pub fn word_occurrence(input_path: &String, field: &String, split: bool) -> HashMap<String, u64>
{
    let mut occurrences: HashMap<String, u64> = HashMap::new();
    let mut reader = csv::Reader::from_path(input_path).unwrap();

    for result in reader.deserialize()
    {
        let article: Article = result.unwrap();
        let field = article.get_field_by_string(&field);

        if split
        {
            let words = field.split(" ");

            for word in words
            {
                *occurrences.entry(word.to_string().to_lowercase()).or_insert(0) += 1;
            }
        }else
        {
                *occurrences.entry(field.to_string().to_lowercase()).or_insert(0) += 1;
        }
    }

    return occurrences
}

fn merge_occurrences(mut master: HashMap<String, u64>, other: HashMap<String, u64>) -> HashMap<String, u64>
{
    let mut value: u64 = 0;
    for key in other.keys()
    {
        value = other[key];
         *master.entry(key.clone()).or_insert(value) += value;
    }

    return master
}

pub fn word_occurrence_in_dir(input_dir: &String, field: &String, split: bool) -> HashMap<String, u64>
{
    let paths: Vec<String> = (0..14).map(|x| format!("{}/{}.csv", input_dir, x).to_string()).collect();

    let mut handles: Vec<JoinHandle<HashMap<String, u64>>> = vec![];
    for path in paths
    {
        let f = field.clone();
        let p = path.clone();
        let handle: JoinHandle<HashMap<String, u64>> = thread::spawn(move || {
            word_occurrence(&p, &f, split)
        });

        handles.push(handle);
    }

    let mut merged_occurrences: HashMap<String, u64> = HashMap::new();

    for handle in handles
    {
        merged_occurrences = merge_occurrences(merged_occurrences, handle.join().unwrap());
    }
    return merged_occurrences
}