use std::fs::File;
use std::io::{BufReader, Write, Lines, BufRead};
use std::path::Path;

pub fn read_lines<P>(filename: P) -> Lines<BufReader<File>>
where P: AsRef<Path>, {
    let file = File::open(filename).unwrap();
    BufReader::new(file).lines()
}


fn rebalance_file(input_path: String, output_files: &mut Vec<File>, start_index: usize) -> usize
{
    let mut index = start_index;
    for line in read_lines(input_path).skip(1) {
        writeln!(output_files[index], "{}", line.unwrap()).unwrap();
        index = (index + 1) % output_files.len();
    }
    return index;
}

fn init_files(input_paths: &Vec<String>, output_files: &mut Vec<File>)
{
    let header = read_lines(input_paths[0].clone()).next().unwrap().unwrap();
    for output in output_files
    {
        writeln!(output, "{}", header).unwrap();
    }
}

pub fn rebalance_dir(input_dir: String, output_dir: String)
{
    let input_paths: Vec<String> = (0..14).map(|x| format!("{}/{}.csv", input_dir, x).to_string()).collect();
    let output_paths: Vec<String> = (0..14).map(|x| format!("{}/{}.csv", output_dir, x).to_string()).collect();

    let mut output_files: Vec<File> = output_paths.iter().map(|x| File::create(x).unwrap()).collect();

    init_files(&input_paths, &mut output_files);

    let mut start_index = 0;
    for path in input_paths
    {
        start_index = rebalance_file(path, &mut output_files, start_index)
    }
}