use regex::Regex;

pub struct Sanitiser
{
    structure: Regex,
    latex: Regex,
    html: Regex,
    whitespaces: Regex,
    amp: Regex
}

impl Sanitiser
{
    pub fn new() -> Sanitiser
    {
        let structure = Regex::new(r"\n|\t").unwrap();
        let latex = Regex::new(r"\\[^{ ]*(\{[^}]*})* |\\[^ ]*").unwrap();
        let html = Regex::new(r"<[^>/]*>|</[^>/]*>").unwrap();
        let whitespaces = Regex::new(r"( {2,})").unwrap();
        let amp = Regex::new(r"&amp(;)?").unwrap();

        Sanitiser {structure, latex, html, whitespaces, amp}
    }

    pub fn sanitise(&self, input: &String) -> String
    {
        let structured = self.structure.replace_all(&input, " ");
        let html_cleaned = self.html.replace_all(&structured, "");
        let latex_cleaned = self.latex.replace_all(&html_cleaned, "");
        let w_cleaned = self.whitespaces.replace_all(&latex_cleaned, " ");
        let and_replaced = self.amp.replace_all(&w_cleaned, "and");


        and_replaced.as_ref().to_string()
    }
}