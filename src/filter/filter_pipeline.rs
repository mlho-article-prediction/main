use crate::article::Article;
use crate::filter::article_filter::ArticleFilter;

pub struct FilterPipeline
{
    filters: Vec<Box<dyn ArticleFilter>>
}

impl FilterPipeline
{
    pub fn new() -> FilterPipeline
    {
        FilterPipeline {filters: vec![]}
    }

    pub fn add_filter(&mut self, filter: Box<dyn ArticleFilter>)
    {
        self.filters.push(filter)
    }

    pub fn run(&self, article: Article) -> Option<Article>
    {
        let mut temp_article = article;
        for filter in &self.filters
        {
            temp_article = match filter.filter(temp_article) {
                Some(x) => x,
                None => return None
            }
        }
        return Some(temp_article)
    }
}