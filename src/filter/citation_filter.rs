use crate::article::Article;
use crate::filter::article_filter::ArticleFilter;

pub struct CitationFilter
{
    lower_bound: u64,
}

impl CitationFilter {
    pub fn new(lower_bound: u64) -> CitationFilter
    {
        CitationFilter { lower_bound}
    }
}

impl ArticleFilter for CitationFilter
{
    fn filter(&self, article: Article) -> Option<Article> {
        if article.get_citations() >= self.lower_bound
        {
            return Some(article);
        }
        return None
    }
}