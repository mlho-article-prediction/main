use whatlang::{Detector, Lang};
use crate::article::Article;
use crate::filter::article_filter::ArticleFilter;

pub struct LangFilter
{
    detector: Detector
}

impl LangFilter
{
    pub fn new() -> LangFilter
    {
        //let allow_list = vec![Lang::Eng];
        let detector = Detector::new();

        LangFilter {detector}
    }
}

impl ArticleFilter for LangFilter
{
    fn filter(&self, article: Article) -> Option<Article>
    {
        if let Some(lang) = self.detector.detect(article.get_title())
        {
            if lang.lang() == Lang::Eng && lang.is_reliable() && lang.confidence() > 0.9
            {
                return Some(article)
            }
        }

        return None
    }
}