use crate::article::Article;
use crate::filter::article_filter::ArticleFilter;

pub struct PublishedFilter
{
    lower_bound: u64,
    upper_bound: u64
}

impl PublishedFilter {
    pub fn new(lower_bound: u64, upper_bound: u64) -> PublishedFilter
    {
        PublishedFilter { lower_bound, upper_bound }
    }
}

impl ArticleFilter for PublishedFilter
{
    fn filter(&self, article: Article) -> Option<Article> {
        if article.get_published() >= self.lower_bound && article.get_published() <= self.upper_bound
        {
            return Some(article);
        }
        return None
    }
}