use crate::article::Article;

pub trait ArticleFilter
{
    fn filter(&self, article: Article) -> Option<Article>;
}