use crate::article::Article;
use crate::filter::article_filter::ArticleFilter;

pub struct AbstractFilter
{

}

impl AbstractFilter {
    pub fn new() -> AbstractFilter
    {
        AbstractFilter {}
    }
}

impl ArticleFilter for AbstractFilter
{
    fn filter(&self, article: Article) -> Option<Article> {
        if article.get_abstract() != ""
        {
            return Some(article);
        }
        return None
    }
}